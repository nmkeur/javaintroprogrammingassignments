/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week5_final_assignments.choice3_sequence_query;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public final class SequenceQuery {

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        SequenceQuery mainObject = new SequenceQuery();
        mainObject.start();
    }

    /**
     * private constructor.
     */
    private SequenceQuery() { }

    /**
     * starts the application.
     */
    private void start() {
        //application logic goes here
        

    }
}